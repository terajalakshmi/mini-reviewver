import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { GraphdataComponent } from './graphdata/graphdata.component';
import { TopnavComponent } from './topnav/topnav.component';
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
  { path: 'navbar', component: NavigationbarComponent },
  { path: 'graph', component: GraphdataComponent },
  { path: 'notes', component:  NotesComponent },
  { path: 'topNav', component:  TopnavComponent  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
